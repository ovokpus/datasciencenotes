# Data Visualization with Python using Matplotlib

This repository provides the data in support of the course Data Visualization with Python using Matplotlib, provided by Cloud Academy.

## Data

We have used two different datasets in this course, which all are available in the data folder. I suggest to follow the following steps in order you to be able to replicate the course steps in your local host.

Open your favourite terminal emulator, and then:

#### 1. Clone the repo
```bash
git clone https://github.com/cloudacademy/data-visualization-with-python-using-matplotlib.git
```
#### 2. Create a python virtualenv
```bash
mkvirtualenv - p python3 <NAME_ENV>
```
#### 3. Install the necessary requirements:
```bash
pip install -r requirements.txt
```
#### 4. Open a jupyter notebook by running:
```bash
jupyter notebook
```
You are now ready to get your hands dirty: enjoy!
