from kafka import KafkaProducer

# Create Message
msg = 'Hello this is a test message'

# Create a producer
producer = KafkaProducer(bootstrap_servers='localhost:9092')


# Function to send messages into Kafka
def kafka_python_producer_async(producer, msg):
    producer.send('mytesttopic5', msg).add_callback(
        on_send_success).add_errback(on_send_error)
    producer.flush()


def on_send_success(record_metadata):
    print(record_metadata.topic)
    print(record_metadata.partition)
    print(record_metadata.offset)


def on_send_error(excp):
    print(excp)


print("Sending message to Kafka")

# Produce the message, but serialize it into bytes first
kafka_python_producer_async(producer, bytes(msg, encoding='utf-8'))

print("Message sent to Kafka")
