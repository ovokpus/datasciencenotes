from kafka import KafkaConsumer


# define a Kafka consumer that waits for messages
def kafka_python_consumer():
    consumer = KafkaConsumer(
        'mytesttopic3',
        bootstrap_servers='localhost:9092',
        auto_offset_reset='earliest',
        enable_auto_commit=True,
        group_id='mypythonconsumer',
        value_deserializer=lambda x: x.decode('utf-8'))

    for message in consumer:
        print("%s:%d:%d: key=%s value=%s" % (
            message.topic, message.partition,
            message.offset, message.key,
            message.value))


print("Starting consumer")

kafka_python_consumer()

print("Finished consumer")
